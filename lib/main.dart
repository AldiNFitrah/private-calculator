import 'package:flutter/material.dart';
import 'package:oprec_ristek_2020/pages/calculator.dart';
import 'package:oprec_ristek_2020/pages/login.dart';
import 'package:oprec_ristek_2020/pages/about.dart';
import 'package:dynamic_theme/dynamic_theme.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new DynamicTheme (
      defaultBrightness: Brightness.light,
        data: (brightness) => new ThemeData(
          primarySwatch: Colors.orange,
          primaryColor: Colors.orange,
          brightness: brightness,
        ),
      themedWidgetBuilder: (context, theme) {
        return new MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Oprec RISTEK Flutter App',
          theme: theme,
          initialRoute: '/',
          routes: {
            '/': (context) => LoginPage(),
            '/calculator': (context) => Calculator(),
            '/about': (context) => About(),
          },
        );
      }
    );
  }
}

/*
DynamicTheme.of(context).setThemeData(ThemeData(
brightness: Brightness.dark,
primaryColor: Colors.orange,
));


 */