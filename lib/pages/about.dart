import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget makeText(String words, Color wordColor, double wordSize, FontWeight wordWeight) {
      return Text(
        words,
        style: TextStyle(
          color: wordColor,
          fontSize: wordSize,
          fontFamily: 'Quicksand',
          fontWeight: wordWeight,
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("About Me"),
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 50, width: 10000,),
          Center(
            child: CircleAvatar(
              radius: 60,
              backgroundImage: AssetImage('assets/img/me.jpg'),
            ),
          ),
          SizedBox(height: 20),
          Center (
            child: makeText("Aldi Naufal Fitrah", Colors.orange[800], 30, FontWeight.w400),
          ),
          Divider(height: 15, thickness: 3, ),
          Padding(
            padding: EdgeInsets.only(left: 20, top: 15),
            child: makeText("Nickname", Colors.orange[400], 17, FontWeight.normal),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, top: 5),
            child: makeText("Aldi", Colors.orange[800], 25, FontWeight.w300),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, top: 15),
            child: makeText("Hobby", Colors.orange[400], 17, FontWeight.normal),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, top: 5),
            child: makeText("Surfing on the internet, cycling, learning new things", Colors.orange[800], 25, FontWeight.w300),
          ),
          Container(
            height: MediaQuery.of(context).size.height*0.25,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    GestureDetector(
                      child: Image.asset(
                        "assets/img/linkedin-logo.png",
                        width: 50,
                      ),
                      onTap: _launchURLLinkedin,
                    ),
                    GestureDetector(
                      child: Image.asset(
                        "assets/img/gitlab-logo.png",
                        width: 50,
                      ),
                      onTap: _launchURLGitlab,
                    ),
                    GestureDetector(
                      child: Image.asset(
                        "assets/img/ig-logo.png",
                        width: 50,
                      ),
                      onTap: _launchURLig,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

_launchURLig() async {
  const url = "https://www.instagram.com/aldinfitrah";
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

_launchURLLinkedin() async {
  const url = "https://www.linkedin.com/in/aldi-naufal-fitrah-645ba4175/";
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

_launchURLGitlab() async {
  const url = "https://www.gitlab.com/aldinfitrah/";
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}