import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController pinController = new TextEditingController();
  int darkTheme = 0;

  @override
  Widget build(BuildContext context) {

    final pinField = TextField(
      autofocus: true,
      controller: pinController,
      keyboardType: TextInputType.number,
      obscureText: true,
      decoration: new InputDecoration(
        icon: Icon(Icons.lock_outline),
        hintText: 'Input the secret PIN',
        labelText: 'PIN *',
        contentPadding: EdgeInsets.symmetric(horizontal: 30),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(100.0)),
      ),
    );

    final loginButton = RaisedButton(
      onPressed: () {
        if(pinController.text == "2020") {
          Navigator.pushReplacementNamed(context, '/calculator');
        } else {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image.asset('assets/img/wakwaw.gif'),
                    Text(
                      "Wrong PIN!!!",
                      style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                        fontSize: 50,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              );
            }
          );
          pinController.clear();
        }
      },
      child: Text("Login"),
      color: Colors.orangeAccent,
    );


    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
        centerTitle: true,
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Oprec RISTEK Aldi'),
              decoration: BoxDecoration(
                color: Colors.orangeAccent,
              ),
            ),
            FlatButton(
              child: Text("Dark Mode"),
              color: Colors.amber[300],
              onPressed: () {
                if(darkTheme == 1) {
                  print(darkTheme);
                  setState(() {
                    darkTheme = 0;
                    DynamicTheme.of(context).setThemeData(ThemeData(
                      brightness: Brightness.light,
                      primaryColor: Colors.orange,
                      primarySwatch: Colors.orange,
                    ));
                  });
                } else {
                  setState(() {
                    darkTheme = 1;
                    print(darkTheme);
                    DynamicTheme.of(context).setThemeData(ThemeData(
                      brightness: Brightness.dark,
                      primaryColor: Colors.orange,
                      primarySwatch: Colors.orange,
                    ));
                  });
                }
              },
            ),
          ],
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Image(
              image: AssetImage("assets/img/ristek20.png"),
              width: MediaQuery.of(context).size.width * 0.3,
            ),
          ),
          SizedBox(height: 25),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18.0),
            child: pinField,
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 25),
                child: loginButton,
              ),
            ],
          ),
          SizedBox(height: 80,),

        ],
      ),
    );
  }
}

