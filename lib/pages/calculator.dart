import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:validators/validators.dart';
import 'package:dynamic_theme/dynamic_theme.dart';


class Calculator extends StatefulWidget {
  @override
  _CalculatorState createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  String equation = "0";
  String result = "0";
  String expression = "";
  double equationFontSize = 38.0;
  double resultFontSize = 48.0;
  int darkTheme = 0;
  @override
  Widget build(BuildContext context) {
    buttonPressed(String buttonChar){
      setState(() {
        if(buttonChar == "C") {
          equation = "0";
          result = "0";
        } else if(buttonChar == "⌫") {
          equation = equation.substring(0, equation.length - 1);
          if(equation == ""){
            equation = "0";
          }
        } else if(buttonChar == "=") {
          expression = equation;
          expression = expression.replaceAll('×', '*');
          expression = expression.replaceAll('÷', '/');

          try{
            Parser p = Parser();
            Expression exp = p.parse(expression);

            ContextModel cm = ContextModel();
            result = '${exp.evaluate(EvaluationType.REAL, cm)}';
          } catch(e){
            result = "Error";
          }
        } else {
          if(equation == "0" && buttonChar != "."){
            if(["0","00"].contains(buttonChar)) {
              equation = "0";
            } else if(isNumeric(buttonChar) || buttonChar == "-") {
              equation = buttonChar;
            }
          } else {
            equation = equation + buttonChar;
          }
        }
        if(result.length > 2 && result.substring(result.length - 2) == ".0") {
          result = result.substring(0, result.length - 2);
        }
      });
    }

    Widget makeButton(String buttonChar, int buttonHeight, Color buttonColor) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.1 * buttonHeight,
        color: buttonColor,
        child: FlatButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
                side: BorderSide(
                  color: Colors.white,
                  width: 1,
                  style: BorderStyle.solid,
                )
            ),
            padding: EdgeInsets.all(16.0),
            onPressed: () => buttonPressed(buttonChar),
            child: Text(
              buttonChar,
              style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.normal,
                  color: Colors.white
              ),
            )
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Private Calculator"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.perm_identity),
            onPressed: () {
              Navigator.pushNamed(context, "/about");
            },
            tooltip: "About me",
          ),
        ],
        centerTitle: true,
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Oprec RISTEK Aldi'),
              decoration: BoxDecoration(
                color: Colors.orangeAccent,
              ),
            ),
            FlatButton(
              child: Text("Dark Mode"),
              color: Colors.amber[300],
              onPressed: () {
                if(darkTheme == 1) {
                  print(darkTheme);
                  setState(() {
                    darkTheme = 0;
                    DynamicTheme.of(context).setThemeData(ThemeData(
                      brightness: Brightness.light,
                      primaryColor: Colors.orange,
                      primarySwatch: Colors.orange,
                    ));
                  });
                } else {
                  setState(() {
                    darkTheme = 1;
                    print(darkTheme);
                    DynamicTheme.of(context).setThemeData(ThemeData(
                      brightness: Brightness.dark,
                      primaryColor: Colors.orange,
                      primarySwatch: Colors.orange,
                    ));
                  });
                }
              },
            ),
          ],
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            child: Text(
              equation,
              style: TextStyle(
                fontSize: 38,
              ),
            ),
            alignment: Alignment.centerRight,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 15),
          ),
          Container(
            child: Text(
              result,
              style: TextStyle(
                fontSize: 48,
                fontWeight: FontWeight.bold,
              ),
            ),
            alignment: Alignment.centerRight,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 15),
          ),
          Expanded(
            child: Divider(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * 0.75,
                child: Table(
                  children: [
                    TableRow(
                        children: [
                          makeButton("C", 1, Colors.redAccent),
                          makeButton("÷", 1, Colors.orangeAccent),
                          makeButton("×", 1, Colors.orangeAccent),
                        ]
                    ),
                    TableRow(
                        children: [
                          makeButton("7", 1, Colors.black45),
                          makeButton("8", 1, Colors.black45),
                          makeButton("9", 1, Colors.black45),
                        ]
                    ),
                    TableRow(
                        children: [
                          makeButton("4", 1, Colors.black45),
                          makeButton("5", 1, Colors.black45),
                          makeButton("6", 1, Colors.black45),
                        ]
                    ),
                    TableRow(
                        children: [
                          makeButton("1", 1, Colors.black45),
                          makeButton("2", 1, Colors.black45),
                          makeButton("3", 1, Colors.black45),
                        ]
                    ),
                    TableRow(
                        children: [
                          makeButton(".", 1, Colors.black45),
                          makeButton("0", 1, Colors.black45),
                          makeButton("00", 1, Colors.black45),
                        ]
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.25,
                child: Table(
                  children: [
                    TableRow(
                        children: [
                          makeButton("⌫", 1, Colors.redAccent)
                        ]
                    ),
                    TableRow(
                        children: [
                          makeButton("-", 1, Colors.orangeAccent)
                        ]
                    ),
                    TableRow(
                        children: [
                          makeButton("+", 2, Colors.orangeAccent)
                        ]
                    ),
                    TableRow(
                        children: [
                          makeButton("=", 1, Colors.redAccent)
                        ]
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

